﻿using UnityEngine;
using System.Collections;

public class ButtonHandler : MonoBehaviour
{
	public string Name;

	void OnEnable()
	{
	}

	public void SetDownState()
	{
		CrossPlatformInputManager.SetButtonDown(Name);
	}

	public void SetUpState()
	{
		CrossPlatformInputManager.SetButtonUp(Name);
	}
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

