﻿using UnityEngine;
using System.Collections;

public class GameManager : Singleton<GameManager> {

	[SerializeField]private GameObject m_CarControllerPrefab;
	[SerializeField]private Transform m_SpawnPoint;

	[SerializeField]private GameObject[] blocksGO;
	[SerializeField]private Vector3 lastPostionBlock;
	[SerializeField]private float blockSize = 60f;
	private int lastBlockAdded = 0;

	private float m_timer;	
	private CarAIControl m_CarController;
	private bool m_bCountDown = false;
	private float m_TimeScale;
	private bool ready;


	public float CurrentTimer { get { return m_timer;} set {m_timer = value;} }
	public float TimeScale { get { return m_TimeScale;} }
	public bool CountDown { get { return m_bCountDown;} }

	public int[] blocksPos = new int[3];
	// Use this for initialization
	void Start () {


		m_TimeScale = 1f;
		m_bCountDown = false;

		//Adiciona os 2 primeiros blocos para teste
		this.GetNextBlocks();
		this.InstantiateBlock(0);
		this.InstantiateBlock(1);

		this.GetNextBlocks();

		GameObject go = Instantiate(m_CarControllerPrefab, m_SpawnPoint.position, m_SpawnPoint.rotation) as GameObject;
		m_CarController = go.GetComponent<CarAIControl>();
		CameraController.Instance.cameraTarget = go.transform;

		WayPointCircuit.Instance.m_carControl = m_CarController;

	}
	
	// Update is called once per frame
	void Update () {


		if (!m_bCountDown)
		{
			m_timer += Time.deltaTime;
		}
		else
		{
			m_timer -= Time.deltaTime;
		}

		if (m_timer < 0)
		{
			m_timer = 0;
		}
	
		if(m_bCountDown && m_timer == 0f)
		{
			CameraToggleZoom();
		}
	}

	public void CameraToggleZoom()
	{
		if (m_timer > 0)
		{
			CameraController.Instance.ToggleZoom();
			ToggleTimeScale();
		}

	}

	private void ToggleTimeScale()
	{
		
		m_TimeScale = m_TimeScale == 1f ? 0.1f : 1f;

		m_bCountDown = false;;

		if (m_TimeScale != 1f) 
		{
			m_bCountDown = true;
		}
			
	}

	public void GetNextBlocks()
	{
		//para testes pega proximos 3 da lista
		for (int i = 0;i < 3; i++)
		{
			if (lastBlockAdded >= blocksGO.Length)
			{
				lastBlockAdded = 0;
			}
			blocksPos[i] = lastBlockAdded;
			lastBlockAdded++;
		}

	}

	void InstantiateBlock (int pos)
	{
		GameObject go = GameObject.Instantiate (blocksGO [blocksPos [pos]], lastPostionBlock, blocksGO [blocksPos [pos]].transform.rotation) as GameObject;
		go.SetActive (true);
		lastPostionBlock = go.transform.position;
		lastPostionBlock.x += blockSize;
		WayPointCircuit.Instance.AddBlock (go.GetComponent<GameBlock> ());

	}

	public bool AddNewBlock(int pos)
	{
		//verifica se encaixa
		if (WayPointCircuit.Instance.ValidPath(blocksGO[blocksPos[pos]].GetComponent<GameBlock>())) 
		{
			InstantiateBlock (pos);
			InGameUI.Instance.HideButton (pos);

			//verifica quantidade de blocos
			if (WayPointCircuit.Instance.Blocks.Count > 3 
				&& m_CarController.CurrentBlock.gameObject != WayPointCircuit.Instance.Blocks[WayPointCircuit.Instance.Blocks.Count - 3].gameObject) {				
				//pega o primeiro ponto do 1 de 3 blocos
				GameBlock b = WayPointCircuit.Instance.Blocks[WayPointCircuit.Instance.Blocks.Count - 3];
				//acelera carro para ficar com apenas 3 blocos
				StartCoroutine(m_CarController.MoveTo(b));
			}

			//Call car do regen path
			m_CarController.RecalculatePath();
			m_CarController.AddSpeed();

			return true;
		}

		m_CarController.RemoveSpeed();
		return false;

	}

	public void RestartGame()
	{
		Application.LoadLevel(0);
	}

	public bool IsReady()
	{
		return this.ready;
	}

	public void SetReady()
	{
		this.ready = true;
		this.m_CarController.SetReady();
		InGameUI.Instance.RestartButtons();
	}

	public CarAIControl CarPlayer
	{
		get{return this.m_CarController;}
	}
}
