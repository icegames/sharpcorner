﻿using UnityEngine;
using System.Collections;

public class GameTouch
{

	private int _id;

	private TouchPhase _phase;

	private Vector2 _pos;

	private Vector2 _lastPos;

	private Vector2 _initialPos;

	private float _startTime;

	public float endTime;

	private int _tapCount;

	private GameObject _owner;

	public int id
	{
		get
		{
			return this._id;
		}
		set
		{
			this._id = value;
		}
	}

	public TouchPhase phase
	{
		get
		{
			return this._phase;
		}
		set
		{
			this._phase = value;
		}
	}

	public Vector2 pos
	{
		get
		{
			return this._pos;
		}
		set
		{
			this._pos = value;
		}
	}

	public Vector2 lastPos
	{
		get
		{
			return this._lastPos;
		}
		set
		{
			this._lastPos = value;
		}
	}

	public Vector2 initialPos
	{
		get
		{
			return this._initialPos;
		}
		set
		{
			this._initialPos = value;
		}
	}

	public float startTime
	{
		get
		{
			return this._startTime;
		}
	}

	public int tapCount
	{
		get
		{
			return this._tapCount;
		}
		set
		{
			this._tapCount = value;
		}
	}

	public bool isTap
	{
		get
		{
			return this.tapCount > 0;
		}
	}
		
	public GameObject owner
	{
		get
		{
			return this._owner;
		}
		set
		{
			this._owner = value;
		}
	}

	public GameTouch(int id)
	{
		this._id = id;
		this._startTime = Time.time;
	}

	private GameTouch()
	{
	}

	public GameTouch Clone()
	{
		GameTouch gameTouch = new GameTouch();
		gameTouch.Copy(this);
		return gameTouch;
	}

	public void Copy(GameTouch other)
	{
		this._id = other._id;
		this._phase = other._phase;
		this._pos = other._pos;
		this._lastPos = other._lastPos;
		this._initialPos = other._initialPos;
		this._startTime = other._startTime;
		this.endTime = other.endTime;
		this._tapCount = other._tapCount;
	}
}

