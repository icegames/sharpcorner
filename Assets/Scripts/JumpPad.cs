using UnityEngine;
using System.Collections;

public class JumpPad : MonoBehaviour
{

	public float jumpForce = 1200f;

	void OnTriggerEnter(Collider other)
	{
		other.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpForce, 0));
	}
}

