﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent(typeof (CarController))]
public class CarAIControl : MonoBehaviour {


	[SerializeField]private float SearchDistance = 20F;
	private float tempDistance = 0F;
	[SerializeField]private float carSpeed = 50f;
	[SerializeField]private Vector3 mass;

	[SerializeField]private float SpeedChange = .1f;

	Path currentPath = null;
	PathManager m_Pathmanager;
	SplineMove m_SplineMove;
	Vector2 nextWayPoint = Vector2.zero;

	private Rigidbody m_Rigidbody;
	private float carCurrentSpeed;
	private float steering;
	private float tempSteering;
	private GameBlock m_CurrentBlock;

	[HideInInspector]
	public bool ready;

	public float CurrentSpeed
	{
		get{return this.carCurrentSpeed;}
	}
	public GameBlock CurrentBlock
	{
		get { return this.m_CurrentBlock; }
		set { this.m_CurrentBlock = value; }
	}

	private void Awake()
	{
		m_Pathmanager = GetComponent<PathManager>();
		m_SplineMove = GetComponent<SplineMove> ();
	}

	// Use this for initialization
	void Start () {
		
		AStart aStart = new AStart();
		Vector2 initPoint = FindInitPoint();
		currentPath = FindBestPath (initPoint, aStart);;
		CreatePathManager ();

		base.StopAllCoroutines ();
	}
	


	private Vector2 FindInitPoint()
	{
		ArrayInt row0 =  WayPointCircuit.Instance.Grid[0];

		for (int i = 0; i < row0.values.Length; i++)
		{
			if (row0.values[i] == 1) 
			{
				return new Vector2(0, i);
			}
		}

		return Vector2.zero;
	}

	private List<Vector2> FindEndPoints()
	{
		int height = WayPointCircuit.Instance.Height;
		ArrayInt row = WayPointCircuit.Instance.Grid[height - 1];
		List<Vector2> l = new List<Vector2>();
		for (int i = 0; i < row.values.Length; i++)
		{
			if (row.values[i] == 1) 
			{
				l.Add( new Vector2(height - 1, i));
			}
		}

		return l;
	}

	public float GetSignedXZAngle(Vector3 vec0, Vector3 vec1)
	{
		float num = Vector3.Angle(vec0, vec1);
		if (Vector3.Cross(vec0, vec1).y > 0f)
		{
			num = -num;

		}
		return num;
	}

	Path FindBestPath (Vector2 gridPos, AStart aStart)
	{
		List<Vector2> endPoints = FindEndPoints ();
		Path tempPath = null;
		Path bestPath = null;
		//procura o melhor path
		for (int i = 0; i < endPoints.Count; i++) {
			tempPath = aStart.Search (gridPos, endPoints [i]);
			if (bestPath == null || tempPath.Waypoints.Count > bestPath.Waypoints.Count) {
				bestPath = tempPath;
			}
		}
		return bestPath;
	}

	public void RecalculatePath()
	{
		//get current grid position
		Vector2 gridPos = WayPointCircuit.Instance.GetGridPosition(m_SplineMove.waypoints [m_SplineMove.currentPoint]);
		AStart aStart = new AStart();

		Path bestPath = FindBestPath (gridPos, aStart);

		if(bestPath.Waypoints.Count > 0)
		{
			currentPath = bestPath;
			CreatePathManager ();
			m_SplineMove.StartMove ();
		}
	}

	void CreatePathManager ()
	{
		m_Pathmanager.waypoints = new Transform[currentPath.Waypoints.Count];
		for (int i = 0; i < currentPath.Waypoints.Count; i++) {
			Transform t = WayPointCircuit.Instance.GridTransform [(int)currentPath.Waypoints [i].x].values [(int)currentPath.Waypoints [i].y].GetComponentInChildren<PathPoint> ().transform;
			m_Pathmanager.waypoints [i] = t;
		}
		m_SplineMove.pathContainer = m_Pathmanager;
	}

	public void SetReady()
	{
		this.ready = true;
		this.m_SplineMove.StartMove ();
	}

	public void AddSpeed()
	{
		carSpeed += SpeedChange;
		m_SplineMove.ChangeSpeed (m_SplineMove.speed + SpeedChange);	
	}

	public void RemoveSpeed()
	{
		carSpeed -= SpeedChange;
		m_SplineMove.ChangeSpeed (m_SplineMove.speed - SpeedChange);

	}

	//faz o carro se mover ate um ponto x
	public IEnumerator MoveTo(GameBlock gb) //turbo speed de 1 segundo
	{
		if (m_CurrentBlock.gameObject == gb.gameObject) {
			yield break;
		}
		float oldSpeed = m_SplineMove.speed;
		//Debug.Log (m_SplineMove.tween.Duration);
		m_SplineMove.ChangeSpeed (m_SplineMove.speed * 2);
		while (true) {
			if (m_CurrentBlock.gameObject == gb.gameObject) {
				break;
			}
			yield return null;
		}
			
		m_SplineMove.ChangeSpeed (oldSpeed);

	}
}
