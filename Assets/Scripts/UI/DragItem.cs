﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class DragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	private RectTransform rectTransform;
	private Transform draggedItemBox;
	private Transform oldSlot;

	public int pos;

	void Start()
	{
		rectTransform = GetComponent<RectTransform>();
		draggedItemBox = GameObject.Find("DraggItem").transform;
		oldSlot = transform.parent;
	}

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		itemBeingDragged = gameObject;
		startPosition = transform.position;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	#endregion

	#region IDragHandler implementation
	public void OnDrag (PointerEventData eventData)
	{
		rectTransform.SetAsLastSibling();
		transform.SetParent(draggedItemBox);
		transform.position = Input.mousePosition;
	}
	#endregion

	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		GameObject DropObject = eventData.pointerEnter;
		itemBeingDragged = null;
		if(DropObject == null || DropObject.transform != oldSlot)
		{			
			if (!GameManager.Instance.AddNewBlock(pos))
			{
				this.ResetPosition();
			}
		}
		else
		{
			this.ResetPosition();
		}
		GetComponent<CanvasGroup>().blocksRaycasts = true;
	}

	#endregion

	public void ResetPosition()
	{
		transform.position = startPosition;
		if (oldSlot != null && transform.parent != oldSlot.transform)
		{
			transform.SetParent(oldSlot.transform);
		}

	}
}
