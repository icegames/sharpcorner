﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InGameUI : Singleton<InGameUI> {

	public Image[] buttons;
	private int totalHide = 0;

	public Button StartButton;
	public Text carSpeedText;

	private float carSpeedValue = -1f;

	public void RestartButtons()
	{
		totalHide = 0;
		for (int i = 0; i < buttons.Length; i++)
		{			
			buttons[i].gameObject.SetActive(true);
			buttons[i].GetComponent<DragItem>().ResetPosition();
			buttons[i].GetComponentInChildren<Text>().text = GameManager.Instance.blocksPos[i].ToString();
		}
	}

	public void HideButton(int pos)
	{
		buttons[pos].gameObject.SetActive(false);
		totalHide++;

		if (totalHide == 3)
		{
			GameManager.Instance.GetNextBlocks();
			RestartButtons();

		}
	}

	public void StartGame()
	{
		
		if (!GameManager.Instance.IsReady())
		{
			StartButton.gameObject.SetActive(false);
			GameManager.Instance.SetReady();
		}
	}

	void Update()
	{
//		if (this.carSpeedValue != GameManager.Instance.CarPlayer.CurrentSpeed)
//		{
//			this.carSpeedValue = GameManager.Instance.CarPlayer.CurrentSpeed;
//		}

//		carSpeedText.text = ((Mathf.FloorToInt(this.carSpeedValue * 100f) / 12)).ToString() + "";
	}
}
