﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUI : MonoBehaviour {

	[SerializeField]private Text m_TimerText;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(CrossPlatformInputManager.GetButtonUp("Jump"))
		{
			GameManager.Instance.CameraToggleZoom();
		}

		UpdateTimer();
	}

	private void UpdateTimer()
	{
		float t = GameManager.Instance.CurrentTimer; 
		int fraction = (int)(t * 100) % 100;
		m_TimerText.text = string.Format ("{0:#00}:{1:00}", t, fraction);
	}


}
