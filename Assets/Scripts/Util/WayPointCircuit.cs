﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WayPointCircuit : Singleton<WayPointCircuit> {

	[SerializeField]
	private List<GameBlock> blocks = new List<GameBlock>(); 
	private int width = 0;
	private int height = 0;

	private List<ArrayInt>  grid = new List<ArrayInt>();
	private List<ArrayTransform> gridTransform = new List<ArrayTransform>();

	public CarAIControl m_carControl;
	private float m_TimerRemoveBlock = 1f; //tempo para remover o ultimo bloco


	public int Width
	{
		get{ return width;}
	}

	public int Height
	{
		get { return height; }
	}

	public List<ArrayTransform> GridTransform
	{
		get
		{
			return gridTransform;
		}
	}

	public List<ArrayInt> Grid
	{
		get
		{
			return grid;
		}
	}

	public List<GameBlock> Blocks
	{
		get { return this.blocks; }
	}
	// Use this for initialization
	void Start () {

		GenerateGrid();
	}
	

	public Vector2 GetGridPosition(Vector3 objectPosition)
	{
		for (int i = 0; i < gridTransform.Count; i++)
		{
			for (int j = 0; j < gridTransform[i].values.Length; j++)
			{				
				PathPoint go = gridTransform [i].values [j].GetComponentInChildren<PathPoint> ();
				if (go) {
					if (go.transform.position.Equals(objectPosition))
					{
						return new Vector2(i, j);
					}
				}
			}
		}

		return new Vector2(-1, -1);
	}


	public Vector3 GetRoutePosition(float dist)
	{
		return Vector3.zero;
	}


	public struct RoutePoint
	{
		public Vector3 position;
		public Vector3 direction;


		public RoutePoint(Vector3 position, Vector3 direction)
		{
			this.position = position;
			this.direction = direction;
		}
	}


	public void RemoveBlock(GameBlock b)
	{
		StartCoroutine (TimerRemoveBlock ());

		for (int i = 0; i < blocks.Count; i++) {
			if (blocks [i].gameObject == b.gameObject) {
				GameBlock bg = blocks [i];
				blocks.RemoveAt (i);
				GameObject.Destroy (bg.gameObject);
				GenerateGrid ();//precisa gerar os grids
			}
		}
	}

	IEnumerator TimerRemoveBlock()
	{
		yield return new WaitForSeconds(m_TimerRemoveBlock);

	}


	public bool ValidPath(GameBlock block)
	{
		//pega o ultimo bloco
		GameBlock lastBloco = blocks[blocks.Count - 1];
		for (int i = 0; i < lastBloco.saidas.Length; i++)
		{
			for (int j = 0; j < block.entradas.Length; j++)
			{
				//verifica as entradas
				if (lastBloco.saidas[i] == block.entradas[j])
				{
					return true;
				}
			}
		}
		
		return false;
	}
	//adiciona um bloco para a lista
	// e refaz o grid
	//valida
	public void AddBlock(GameBlock block)
	{
		blocks.Add(block);
		GenerateGrid();
			
	}



	//le todos o blocos e o grid
	private void GenerateGrid()
	{
		if (blocks.Count > 0)
		{		
			grid = new List<ArrayInt>();
			gridTransform = new List<ArrayTransform>();
			for (int i = 0; i < blocks.Count; i++)
			{
				ArrayInt[] map = blocks[i].map;
				ArrayTransform[] mapT = blocks[i].mapTransform;
				for (int j = 0; j < map.Length; j++)
				{
					grid.Add(map[j]);
					gridTransform.Add(mapT[j]);
				}
			}

			this.width = grid[0].values.Length;
			this.height = grid.Count;
		}


	}
		


}
