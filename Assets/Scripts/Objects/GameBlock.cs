﻿using UnityEngine;
using System.Collections;

public class GameBlock : MonoBehaviour {

	public ArrayInt[] map;
	public ArrayTransform[] mapTransform;

	public int[] entradas; 
	public int[] saidas;


	private void OnDrawGizmos()
	{
		if (map.Length > 1 )
		{
			Gizmos.color = Color.yellow;
			Vector3 prev = Vector3.zero;
			for (int i = 0; i < map.Length; i++)
			{
				for(int j = 0; j < map[i].Length; j++)
				{
					if(map[i].values[j] == 1)
					{
						Vector3 next = mapTransform[i].values[j].position;
						next.y += 8;
						if (prev != Vector3.zero)
						{
							Gizmos.DrawLine(prev, next);
						}
						Gizmos.DrawSphere(next, 2f);
						prev = next;
					}
				}
			}
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") {
			CarAIControl player = col.GetComponent<CarAIControl> ();
			if (player) {
				player.CurrentBlock = this;
			} else {
				player = col.transform.parent.GetComponent<CarAIControl> ();
				player.CurrentBlock = this;
			}
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.tag == "Player") {
			StartCoroutine (DestroyMe());	
		}
	}

	IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds (2f);//futuramente fazer um calculo pela velocidade do player
		WayPointCircuit.Instance.RemoveBlock(this);
	}
}
