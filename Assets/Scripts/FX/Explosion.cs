﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	[SerializeField]private float force;
	[SerializeField]private float radius;
	// Use this for initialization
	void Start () {

		Collider[] colliders = GetComponentsInChildren<Collider>();

		foreach (Collider c in colliders)
		{
			c.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, radius, 1, ForceMode.Impulse);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
