﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(GameBlock))]
public class MapInspector : Editor
{
	int firstDimensionSize;
	int secondDimensionSize;
	bool editMode;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		GameBlock someClass = (GameBlock)target;

		if(CanCreateNewArray()) CreateNewArray(someClass);

		SetupArray(someClass);
	}

	bool CanCreateNewArray()
	{
		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Create New Array")) editMode = true;
		if(GUILayout.Button("Cancel")) editMode = false;
		EditorGUILayout.EndHorizontal();

		return editMode;
	}

	void CreateNewArray(GameBlock someClass)
	{
		GetDimensions();
		if(ConfirmedCanCreate()) CreateArray(someClass);
	}

	void GetDimensions()
	{
		firstDimensionSize = EditorGUILayout.IntField("First Dimension Size", firstDimensionSize);
		secondDimensionSize = EditorGUILayout.IntField("Second Dimension Size", secondDimensionSize);
	}

	bool ConfirmedCanCreate()
	{		

		EditorGUILayout.BeginHorizontal();
		bool canCreate = (GUILayout.Button("Create New Multidimensional Array"));
		EditorGUILayout.EndHorizontal();

		if(canCreate)
		{
			editMode = false;
			return true;
		}
		return false;
	}

	void CreateArray(GameBlock someClass)
	{
		someClass.map = new ArrayInt[firstDimensionSize];
		for(int i = 0; i < firstDimensionSize; i++)
		{
			someClass.map[i] = new ArrayInt(secondDimensionSize);
		}
	}

	void SetupArray(GameBlock someClass)
	{
		if(someClass.map != null && someClass.map.Length > 0)
		{
			for(int i = 0; i < someClass.map.Length; i++)
			{
				EditorGUILayout.BeginHorizontal();

				for(int j = 0; j < someClass.map[i].Length; j++)
				{
					someClass.map[i][j] = EditorGUILayout.IntField(someClass.map[i][j], GUILayout.Width(20));
				}

				EditorGUILayout.EndHorizontal();
			}
		}
	}
}

